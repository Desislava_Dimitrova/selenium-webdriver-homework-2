package pages.jira;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.List;

import static com.telerikacademy.testframework.Utils.*;

public class JiraStoryPage extends JiraBoardPage {

    String storyName = getConfigPropertyByKey("jira.storyName");

    public JiraStoryPage(WebDriver driver) {
        super(driver, "jira.boardPage");
    }

    public void createStory(){

        actions.waitForElementClickable("jira.boardPage.createIssueButton");
        actions.clickElement("jira.boardPage.createIssueButton");
        actions.waitForElementClickable("jira.createIssue.dropDownButton");
        actions.clickElement("jira.createIssue.dropDownButton");
        actions.waitForElementClickable("jira.createStory.dropDownButtonStory");
        actions.clickElement("jira.createStory.dropDownButtonStory");

    }

    public void storyDetails() {

        String storyDescription = getConfigPropertyByKey("jira.storyDescription");

        actions.waitForElementPresent("jira.createIssue.summaryField");
        actions.typeValueInField(storyName,"jira.createIssue.summaryField");
        actions.typeValueInField(storyDescription,"jira.createIssue.description");
        actions.clickElement("jira.priority.dropDownButton");
        actions.waitForElementVisible("jira.priority.setPriorityHighest");
        actions.clickElement("jira.priority.setPriorityHighest");
        actions.clickElement("jira.createIssue.createButton");


    }

    public String assertStoryExists() {

        actions.waitForElementPresent("jira.createdNotification.notificationPopup");
        List elements = getWebDriver().findElements(By.xpath(getUIMappingByKey("jira.createdNotification.notificationPopup")));

        Assert.assertTrue("Story was not created",elements.size() > 0 );
        String confirmationNotification = getWebDriver().findElement(By.xpath(getUIMappingByKey("jira.createdNotification." +
                "notificationPopup"))).getText();
        String storyID = confirmationNotification.split("\"")[1];


        return storyID;
    }

    public void deleteStory(String storyID){

        actions.clickElement("jira.deleteStory.selectIssue", storyID);
        actions.clickElement("jira.createdNotification.notificationPopup");
        actions.clickElement("jira.createIssue.viewIssue");
        actions.waitForElementPresent("jira.createdIssue.actionButton");
        actions.clickElement("jira.createdIssue.actionButton");
        actions.waitForElementPresent("jira.createdIssue.deleteButton");
        actions.clickElement("jira.createdIssue.deleteButton");
        actions.waitForElementPresent("jira.createdIssue.deleteButtonConfirmation");
        actions.clickElement("jira.createdIssue.deleteButtonConfirmation");
    }



}
