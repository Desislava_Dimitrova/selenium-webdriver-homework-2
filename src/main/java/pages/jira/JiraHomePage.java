package pages.jira;

import com.telerikacademy.testframework.pages.BasePageJira;
import org.openqa.selenium.WebDriver;


public class JiraHomePage extends BasePageJira {

    public JiraHomePage(WebDriver driver, String pageUrlKey) {super(driver, "jira.homePage");
    }
}
