package pages.jira;

import org.openqa.selenium.WebDriver;

import static com.telerikacademy.testframework.Utils.getConfigPropertyByKey;

public class JiraBoardPage extends JiraHomePage {

    public JiraBoardPage(WebDriver driver, String s) {
        super(driver, "jira.homePage");
    }

    public void loginUser(String userKey) {
        String username = getConfigPropertyByKey("jira.users." + userKey + ".username");
        String password = getConfigPropertyByKey("jira.users." + userKey + ".password");

        navigateToPage();
        assertPageNavigated();

        actions.waitForElementVisible("jira.loginPage.myAccountButton");
        actions.clickElement("jira.loginPage.myAccountButton");
        actions.waitForElementClickable("jira.loginPage.logInButton");
        actions.clickElement("jira.loginPage.logInButton");


        actions.typeValueInField(username, "jira.loginPage.username");
        actions.clickElement("jira.loginPage.continueButton");
        actions.typeValueInField(password, "jira.loginPage.password");
        actions.clickElement("jira.loginPage.accountLogInButton");

    }
    public void navigateToBoard() {


        actions.waitForElementClickable("jira.board.displayButton");
        actions.clickElement("jira.board.displayButton");
        actions.waitForElementClickable("jira.boardPage");
        actions.clickElement("jira.boardPage");

    }
}
