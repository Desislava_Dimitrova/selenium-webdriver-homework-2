package pages.jira;

import com.telerikacademy.testframework.PropertiesManager;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.List;
import java.util.Properties;

import static com.telerikacademy.testframework.Utils.*;
import static org.apache.commons.io.FileUtils.waitFor;

public class JiraBugPage extends JiraBoardPage {

    public JiraBugPage(WebDriver driver) {
        super(driver, "jira.boardPage");
    }

    String storyID;
    String bugID;
    public void createBug(){

        actions.waitForElementClickable("jira.boardPage.createIssueButton");
        actions.clickElement("jira.boardPage.createIssueButton");

        actions.waitForElementClickable("jira.createIssue.dropDownButton");
        actions.clickElement("jira.createIssue.dropDownButton");
        actions.waitForElementClickable("jira.createStory.dropDownButtonBug");
        actions.clickElement("jira.createStory.dropDownButtonBug");

    }
    public void bugDetails() {

        String bugName = getConfigPropertyByKey("jira.bugName");
        String bugDescription = getConfigPropertyByKey("jira.bugDescription");


        actions.typeValueInField(bugName,"jira.createIssue.summaryField");
        actions.typeValueInField(bugDescription,"jira.createIssue.description");
        actions.clickElement("jira.priority.dropDownButton");
        actions.waitForElementVisible("jira.priority.setPriorityHighest");
        actions.clickElement("jira.priority.setPriorityHighest");
        actions.clickElement("jira.createIssue.createButton");
    }

    public static String getUrlMappingByKeyWithParameter(String key, String name) {
        Properties uiMappings = PropertiesManager.PropertiesManagerEnum.INSTANCE.getUiMappings();
        String value = uiMappings.getProperty(key);
        return String.format(value, name);
    }

    public void linkBugToStory(String storyID, String bugID){
        actions.waitFor(2000);
        System.out.println(bugID);
        actions.waitForElementVisible("jira.createdNotification.notificationPopup");
        driver.get(getUrlMappingByKeyWithParameter("jira.createdIssue.Url", storyID));
        actions.waitForElementClickable("jira.linkIssue.selectButton");
        actions.clickElement("jira.linkIssue.selectButton");
        actions.waitForElementClickable("jira.searchForIssue.field");
        actions.clickElement("jira.searchForIssue.field");
        actions.locateElementAndPressEnter(bugID);
//        actions.waitFor(2000);
//        actions.waitForElementVisible("jira.linkIssue.linkButton");
        actions.clickElement("jira.linkIssue.linkButton");

    }

// TODO correct assert
    public String assertBugExists() {

        actions.waitForElementPresent("jira.createdNotification.notificationPopup");
        List elements = getWebDriver().findElements(By.xpath(getUIMappingByKey("jira.createdNotification.notificationPopup")));

        Assert.assertTrue("Bug was not created",elements.size() > 0 );
        String confirmationNotification = getWebDriver().findElement(By.xpath(getUIMappingByKey("jira.createdNotification." +
                "notificationPopup"))).getText();
        String bugID = confirmationNotification.split("\"")[1];


        return bugID;
    }
    public void deleteBug(String bugID){

        actions.clickElement("jira.deleteStory.selectIssue", bugID);
        actions.clickElement("jira.createdNotification.notificationPopup");
        actions.clickElement("jira.createIssue.viewIssue");
        actions.waitForElementPresent("jira.createdIssue.actionButton");
        actions.clickElement("jira.createdIssue.actionButton");
        actions.waitForElementPresent("jira.createdIssue.deleteButton");
        actions.clickElement("jira.createdIssue.deleteButton");
        actions.waitForElementPresent("jira.createdIssue.deleteButtonConfirmation");
        actions.clickElement("jira.createdIssue.deleteButtonConfirmation");
    }
}
