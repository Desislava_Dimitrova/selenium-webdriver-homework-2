package test.cases.jira;

import com.telerikacademy.testframework.UserActions;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import pages.jira.JiraBoardPage;

public class BaseTest {
    UserActions actions = new UserActions();

    @BeforeClass
    public static void setUp() {

        UserActions.loadBrowser("jira.homePage");

    }


    public void login() {

        JiraBoardPage loginPage = new JiraBoardPage(actions.getDriver(), "jira.boardPage");
        loginPage.loginUser("user");
    }


    @Test
    public void loginInJiraTest() {

        login();

    }
}
