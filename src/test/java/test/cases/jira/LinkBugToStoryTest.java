package test.cases.jira;

import com.telerikacademy.testframework.UserActions;
import org.junit.Test;
import pages.jira.JiraBugPage;
import pages.jira.JiraStoryPage;

public class LinkBugToStoryTest extends BaseTest{
    @Test
    public void linkBugToStory() {
        String storyID;
        String bugID;
        login();

        JiraStoryPage jiraStoryPage = new JiraStoryPage(actions.getDriver());
        jiraStoryPage.navigateToBoard();
        jiraStoryPage.createStory();
        jiraStoryPage.storyDetails();
        storyID = jiraStoryPage.assertStoryExists();

        JiraBugPage jiraBugPage = new JiraBugPage(actions.getDriver());
        actions.waitFor(10000);
        jiraBugPage.createBug();
        jiraBugPage.bugDetails();
        bugID = jiraBugPage.assertBugExists();

        jiraBugPage.linkBugToStory(storyID,bugID);

    }
}
