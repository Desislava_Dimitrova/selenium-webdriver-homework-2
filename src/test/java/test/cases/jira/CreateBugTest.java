package test.cases.jira;

import com.telerikacademy.testframework.UserActions;
import org.junit.Test;
import pages.jira.JiraBugPage;
import pages.jira.JiraStoryPage;


public class CreateBugTest extends BaseTest{


    @Test
    public void createNewBugWhenBoardIsDisplayed() {

        login();

        JiraBugPage jiraBugPage = new JiraBugPage(actions.getDriver());
        jiraBugPage.navigateToBoard();
        jiraBugPage.createBug();
        jiraBugPage.bugDetails();
        actions.waitFor(1000);
        String bugID = jiraBugPage.assertBugExists();
        jiraBugPage.deleteBug(bugID);
        UserActions.quitDriver();
    }
}
