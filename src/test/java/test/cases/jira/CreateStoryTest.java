package test.cases.jira;

import com.telerikacademy.testframework.UserActions;
import org.junit.Test;
import pages.jira.JiraStoryPage;


public class CreateStoryTest extends BaseTest{

    @Test
    public void createNewStoryWhenBoardIsDisplayed() {
        String storyID;
        login();

        JiraStoryPage jiraStoryPage = new JiraStoryPage(actions.getDriver());
        jiraStoryPage.navigateToBoard();
        jiraStoryPage.createStory();
        jiraStoryPage.storyDetails();
        storyID = jiraStoryPage.assertStoryExists();
        jiraStoryPage.deleteStory(storyID);
        UserActions.quitDriver();

    }
}
